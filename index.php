<?php

include_once 'vendor/autoload.php';



$json = file_get_contents('php://input');

if($json == null){
    $response = [
        'success'   => false,
        'message'   => 'datos incorrectos',
        'cae'       => '',
        'caeFecha'  => ''];

    http_response_code(400);
    echo json_encode($response);
    exit();
}

$dataJson = json_decode($json);

/*
$data = array(
    'CantReg' 	=> intval($dataJson->cantReg),  // Cantidad de comprobantes a registrar
    'PtoVta' 	=> intval($dataJson->ptoVenta),  // Punto de venta
    'CbteTipo' 	=> intval($dataJson->comprobanteTipo),  // Tipo de comprobante (ver tipos disponibles)
    'Concepto' 	=> intval($dataJson->concepto),  // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
    'DocTipo' 	=> intval($dataJson->docTipo), // Tipo de documento del comprador (99 consumidor final, ver tipos disponibles)
    'DocNro' 	=> intval($dataJson->docNumero),  // Número de documento del comprador (0 consumidor final)
    'CbteDesde' 	=> intval($dataJson->comprobanteDesde),  // Número de comprobante o numero del primer comprobante en caso de ser mas de uno
    'CbteHasta' 	=> intval($dataJson->comprobanteHasta),  // Número de comprobante o numero del último comprobante en caso de ser mas de uno
    'CbteFch' 	=> $dataJson->comprobanteFecha, //intval(date('Ymd')), // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
    'ImpTotal' 	=> $dataJson->importeTotal, // Importe total del comprobante
    'ImpTotConc' 	=> $dataJson->importeNeto,   // Importe neto no gravado
    'ImpNeto' 	=> $dataJson->importeNetoGravado, // Importe neto gravado
    'ImpOpEx' 	=> $dataJson->importeExentoIva,   // Importe exento de IVA
    'ImpIVA' 	=> $dataJson->importeTotalIva,  //Importe total de IVA
    'ImpTrib' 	=> $dataJson->importeTotalTributo,   //Importe total de tributos
    'MonId' 	=> $dataJson->monedaId, //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos)
    'MonCotiz' 	=> intval($dataJson->montoCotizacion),     // Cotización de la moneda usada (1 para pesos argentinos)
    'Iva' 			=> array( // (Opcional) Alícuotas asociadas al comprobante
		array(
			'Id' 		=> 5, // Id del tipo de IVA (ver tipos disponibles)
			'BaseImp' 	=> 100, // Base imponible
			'Importe' 	=> 21 // Importe
		)
	)
);*/

$data = array(
    'CantReg' 		=> 1, // Cantidad de comprobantes a registrar
    'PtoVta' 		=> 1, // Punto de venta
    'CbteTipo' 		=> 6, // Tipo de comprobante (ver tipos disponibles)
    'Concepto' 		=> 1, // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
    'DocTipo' 		=> 80, // Tipo de documento del comprador (ver tipos disponibles)
    'DocNro' 		=> 20111111112, // Numero de documento del comprador
    'CbteDesde' 	=> 1, // Numero de comprobante o numero del primer comprobante en caso de ser mas de uno
    'CbteHasta' 	=> 1, // Numero de comprobante o numero del ultimo comprobante en caso de ser mas de uno
    'CbteFch' 		=> intval(date('Ymd')), // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
    'ImpTotal' 		=> 176.25, // Importe total del comprobante
    'ImpTotConc' 	=> 0, // Importe neto no gravado
    'ImpNeto' 		=> 150, // Importe neto gravado
    'ImpOpEx' 		=> 0, // Importe exento de IVA
    'ImpIVA' 		=> 26.25, //Importe total de IVA
    'ImpTrib' 		=> 0, //Importe total de tributos
    'FchServDesde' 	=> NULL, // (Opcional) Fecha de inicio del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
    'FchServHasta' 	=> NULL, // (Opcional) Fecha de fin del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
    'FchVtoPago' 	=> NULL, // (Opcional) Fecha de vencimiento del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
    'MonId' 		=> 'PES', //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos)
    'MonCotiz' 		=> 1, // Cotización de la moneda usada (1 para pesos argentinos)
    'CbtesAsoc' 	=> array( // (Opcional) Comprobantes asociados
        array(
            'Tipo' 		=> 6, // Tipo de comprobante (ver tipos disponibles)
            'PtoVta' 	=> 1, // Punto de venta
            'Nro' 		=> 1, // Numero de comprobante
            'Cuit' 		=> 20111111112 // (Opcional) Cuit del emisor del comprobante
        )
    ),

    'Iva' 			=> array( // (Opcional) Alícuotas asociadas al comprobante
        array(
            'Id' 		=> 5, // Id del tipo de IVA (ver tipos disponibles)
            'BaseImp' 	=> 150, // Base imponible
            'Importe' 	=> 21 // Importe
        )
    ),
);

//print_r($data);
$afip = new Afip(array('CUIT' => 20290108692, 'production'=> false) );
//$afip = new Afip(array('CUIT' => 30692975533, 'production'=> true) );
//var_dump($afip->ElectronicBilling->GetLastVoucher(600,6));

$res = $afip->ElectronicBilling->CreateVoucher($data);


//var Global ptoVenta

//badrequest
http_response_code(400);

$response = [
    'success'   => true,
    'message'   => '',
    'cae'       => $res['CAE'], //CAE asignado el comprobante
    'caeFecha'  => $res['CAEFchVto'] //Fecha de vencimiento del CAE (yyyy-mm-dd)
];

echo json_encode($response);

?>
